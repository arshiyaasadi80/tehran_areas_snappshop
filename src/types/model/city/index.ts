export interface ICity {
  city: 'tehran' | string
}

export interface IArea {
  name: string
  region: number
  id: number
  orderItem: number
}
