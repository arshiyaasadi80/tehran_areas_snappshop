import type { NextApiResponse } from 'next'
import { ICity, IArea } from '@/types/model/city'

export interface ICityResDTO extends NextApiResponse {
  city: ICity
  areas: IArea[]
}
