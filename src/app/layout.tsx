import '@/app/globals.sass'

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="fa" className="bg-whitesmoke">
      <body id="root-layout" className="w-full max-w-md mx-auto bg-white">
        {children}
      </body>
    </html>
  )
}
