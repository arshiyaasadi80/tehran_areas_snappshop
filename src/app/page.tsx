'use client'

import React, { useState } from 'react'
import BottomSheet from '@/app/components/BottomSheet'
import SearchBox from '@/app/components/SearchBox'
import List from '@/app/components/List/List'
import Item from '@/app/components/List/Item'
import useDebounce from '@/hooks/UseDebounce'
import { getAreas } from '@/services/index'
import { IArea } from '@/types/model/city'

export default function Page() {
  // states
  const city = 'tehran'
  const [searchValue, setSearchValue] = useState<string>('')
  const [isBottomSheetOpen, setIsBottomSheetOpen] = useState<boolean>(false)
  const [areaaList, setAreaaList] = useState<IArea[]>([])
  const [itemSelected, setItemSelected] = useState<number | string | null>()
  const [isLoading, setIsloading] = useState<boolean>()

  // handel OnChange
  const handelOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value)
  }

  // handel SearchBoxClear
  const handeSearchBoxClear = () => {
    setSearchValue('')
    setItemSelected(null)
    setIsBottomSheetOpen(false)
  }

  // DeBounce hoock
  useDebounce(
    () => {
      setIsloading(true)
      // get 'Tehran' Areas Data
      getAreas(city, searchValue)
        .then((response) => {
          response.json().then((data) => setAreaaList(data?.areas))
        })
        .finally(() => {
          setInterval(() => setIsloading(false), 700)
        })
    },
    // wathch searchValue state
    [searchValue],
    700
  )

  // boxSize
  // 1.5rem 1.75rem

  return (
    <div className="w-full h-full bg-white flex gap-2 flex-col p-2">
      <header className="bg-with shadow-sm">
        <SearchBox
          onChange={(e) => handelOnChange(e)}
          onFockes={() => setIsBottomSheetOpen(true)}
          clear={() => handeSearchBoxClear()}
          back={() => setIsBottomSheetOpen(false)}
          placeHolder={
            <span className="text-gray flex">
              جستجو در <span className="text-purple"> شهر تهران </span>
            </span>
          }
        />
      </header>
      <div className="h-full relative">
        <BottomSheet isOpen={isBottomSheetOpen}>
          <List height={`${areaaList.length * 36}px`} loading={isLoading}>
            {areaaList &&
              areaaList.map((item) => {
                return (
                  <Item
                    key={item.orderItem}
                    item={item}
                    isSelect={itemSelected === `${item.orderItem}->${item.name}`}
                    onClick={() => setItemSelected(`${item.orderItem}->${item.name}`)}
                  />
                )
              })}
          </List>
        </BottomSheet>
      </div>
    </div>
  )
}
