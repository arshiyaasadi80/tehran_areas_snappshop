'use client'

import { IArea } from '@/types/model/city'

interface IBottemSheet {
  item: IArea
  isSelect?: boolean
  onClick?: () => void
}
const Item: React.FC<IBottemSheet> = (props) => {
  // props value
  const { item, isSelect, onClick } = props

  return (
    <li
      className="w-full min-h-[1.75rem] max-h-[1.75rem] flex items-center justify-between cursor-pointer"
      onClick={() => onClick && onClick()}
    >
      <span>{item.name}</span>
      <span
        className={`w-5 h-5 rounded-full flex items-center justify-center border-2  border-solid 
                                  ${isSelect ? 'border-purple' : 'border-gray'}`}
      >
        <span className={`rounded-full ${isSelect && 'bg-purple w-3 h-3'}`}></span>
      </span>
    </li>
  )
}
export default Item
