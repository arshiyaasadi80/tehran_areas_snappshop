'use client'

interface IBottemSheet {
  children: React.ReactNode
  height?: string
  loading?: boolean
}
const List: React.FC<IBottemSheet> = (props) => {
  // props value
  const { height, loading, children } = props

  return (
    <ul
      className={`w-full flex flex-col gap-6 p-2 divide-y divide-gray-400 ${loading && 'animate-pulse'}`}
      style={{ height }}
    >
      {loading
        ? Array(12)
            .fill('')
            .map((item, index) => {
              return (
                <div
                  key={`${index}-${item}`}
                  className=" min-h-[1.75rem] max-h-[1.75rem] bg-white rounded-full w-[70%]"
                ></div>
              )
            })
        : children}
    </ul>
  )
}
export default List
