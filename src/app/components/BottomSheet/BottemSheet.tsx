'use client'

interface IBottemSheet {
  children: React.ReactNode
  isOpen: boolean
}
const BottemSheet: React.FC<IBottemSheet> = (props) => {
  // props value
  const { children, isOpen } = props

  return (
    <div
      className={`transition ease-in-out w-full bg-whitesmoke flex gap-2 items-center justify-center rounded-md absolute left-0 right-0 bottom-0 overflow-hidden
                  ${isOpen ? 'top-0' : 'top-[100%] translate-y-[50%] opacity-0'}`}
    >
      <div className="w-full h-full overflow-auto">{children}</div>
    </div>
  )
}
export default BottemSheet
