'use client'

import Image from 'next/image'
import { useState } from 'react'

interface ISearchBox {
  value?: string
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void
  onFockes?: (e?: React.ChangeEvent<HTMLInputElement>) => void
  clear?: () => void
  back?: () => void
  placeHolder?: any
}

const SearchBox: React.FC<ISearchBox> = (props) => {
  // components
  const { onChange, onFockes, clear, placeHolder, back, value = '' } = props

  // states
  const [inputValue, setInputValue] = useState<string>(value)
  const [inputStatus, setInputStatus] = useState<string>('search')

  // icons path
  const icons = {
    search: '/icons/search.svg',
    back: '/icons/back.svg',
  }

  // set input value
  const handelOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value },
    } = e

    setInputValue(value)

    if (value) setInputStatus('back')
    else if (!value) setInputStatus('search')

    if (onChange) return onChange(e)
  }

  // handel onFocus
  const handelOnFocus = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputStatus('back')
    if (onFockes) return onFockes(e)
  }

  // clear inputh value
  const handelClear = () => {
    setInputValue('')
    if (clear) return clear()
  }

  // clear handelback
  const handelback = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault
    setInputStatus('search')
    if (back) return back()
  }

  return (
    <div className="w-full bg-whitesmoke rounded-md">
      <label
        htmlFor="SearchBox"
        className="relative p-4 w-full flex gap-2 items-center justify-between h-full cursor-pointer"
      >
        <button className="cursor-pointer flex items-center justify-center" onClick={(e) => handelback(e)}>
          <Image width="24" height="24" src={icons[inputStatus]} alt="icon" />
        </button>

        {inputStatus === 'search' && placeHolder && <span className="absolute right-12">{placeHolder}</span>}

        <input
          className={`w-full h-full ${inputStatus === 'search' && 'opacity-0'}`}
          type="text"
          name="SearchBox"
          id="SearchBox"
          value={inputValue}
          onChange={(e) => handelOnChange(e)}
          onFocus={(e) => handelOnFocus(e)}
        />

        {inputStatus === 'back' && inputValue && (
          <button className="max-w-max max-h-max" onClick={handelClear}>
            <Image width="24" height="24" src="/icons/close.svg" alt="close icon" />
          </button>
        )}
      </label>
    </div>
  )
}
export default SearchBox
