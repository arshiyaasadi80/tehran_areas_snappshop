import type { ICityReqDTO, ICityResDTO } from '@/types/DTO/city'
import type { IArea } from '@/types/model/city'

export default function getCity(req: ICityReqDTO, res: ICityResDTO): void {
  if (req.method !== 'GET') res.status(500)

  // get query datas
  const { city, areas = '' } = req.query

  // import simpel datas
  const cityData: { areas: IArea[] } = require('@/utils/mock/city.json')

  // filter datas
  const newCityData = cityData.areas.filter((area) => {
    return area.name.match(String(areas))
  })

  //
  res.status(200).json({
    city,
    areas: newCityData,
  })
}
