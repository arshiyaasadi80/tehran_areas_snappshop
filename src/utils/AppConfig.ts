export const AppConfig = {
  site_name: 'Tehran Areas',
  title: 'Tehran Areas SnappShop',
  description: 'Tehran Areas: Starter code for your Nextjs Boilerplate with Tailwind CSS',
  locale: 'fa',
}
