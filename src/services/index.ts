// get 'Tehran' Areas Data
export const getAreas = async (city: string, areas: string) => {
  return await fetch(`api/city/${city}?areas=${areas}`)
}
