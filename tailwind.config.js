/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  mode: 'jit',
  content: [
    './src/app/**/*.{js,ts,jsx,tsx}',
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: 'class',
  theme: {
    container: {},
    screens: {
      'sm': { 'min': '320px', 'max': '480px' },
      'md': { 'min': '480px', 'max': '768px' },
      'lg': { 'min': '769px', 'max': '1024px' },
      'xl': { 'min': '1025px', 'max': '1200px' },
    },
    fontFamily: {
      sans: ['Graphik', 'sans-serif'],
    },
    fontSize: {
      'sm': '0.8rem',
      'base': '1rem',
      'xl': '1.25rem',
      '2xl': [
        '1.5rem',
        {
          lineHeight: '2rem',
          letterSpacing: '-0.01em',
          fontWeight: '500',
        },
      ],
      '3xl': [
        '1.875rem',
        {
          lineHeight: '2.25rem',
          letterSpacing: '-0.02em',
          fontWeight: '700',
        },
      ],
      '4xl': '2.441rem',
      '5xl': '3.052rem',
    },
    colors: {
      'transparent': 'transparent',
      'whitesmoke': 'rgb(var(--color-whitesmoke) / <alpha-value>)',
      'white': 'rgb(var(--color-white) / <alpha-value>)',
      'purple': 'rgb(var(--color-purple) / <alpha-value>)',
      'gray': 'rgb(var(--color-gray) / <alpha-value>)',
      'primary': 'rgb(var(--color-primary) / <alpha-value>)',
      'secondary': 'rgb(var(--color-secondary) / <alpha-value>)',
      'accent': 'rgb(var(--color-accent) / <alpha-value>)',
      'neutral': 'rgb(var(--color-neutral) / <alpha-value>)',
      'base': 'rgb(var(--color-base) / <alpha-value>)',
      'info': 'rgb(var(--color-info) / <alpha-value>)',
      'success': 'rgb(var(--color-success) / <alpha-value>)',
      'warning': 'rgb(var(--color-warning) / <alpha-value>)',
      'error': 'rgb(var(--color-error) / <alpha-value>)',
      ...defaultTheme.colors,
    },
    opacity: {
      '0': '0',
      '20': '0.2',
      '40': '0.4',
      '60': '0.6',
      '80': '0.8',
      '100': '1',
    },
    extend: {
      content: {
        'link': 'url("/icon/link.svg")',
      },
      fontFamily: {
        Montserrat: ['Montserrat', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [
    // ...
  ],
}
